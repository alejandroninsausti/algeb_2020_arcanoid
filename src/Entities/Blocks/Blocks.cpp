#include "Blocks.h"

namespace game
{
	namespace blocks {
		const short maxblocks = 50;
		short aux_blocks = 0;
		Blocks blocksLevel[maxblocks];

		void setPropertiesBlocks(const int screenWidth) 
		{
			Color cant_colors_blocks[5]{ WHITE,RED,BLUE,GREEN,YELLOW };
			const short cant_filas = 5;
			const short width = 60;
			const short height = 20;
			const float y_mod = 100;
			int aux_color = 0;

			//set j to the first y position, then add block height plus extra
			for (short j = GetScreenHeight() / y_mod; j < height * cant_filas + GetScreenHeight() / y_mod; j += height)
			{
				//same with i and x, but using width
				for (short i = screenWidth / 100; i < screenWidth - screenWidth / 100; i += width)
				{
					if (aux_blocks < maxblocks)
					{
						blocksLevel[aux_blocks].color = cant_colors_blocks[aux_color];
						blocksLevel[aux_blocks].active = true;

						blocksLevel[aux_blocks].properties.x = i;
						blocksLevel[aux_blocks].properties.y = j;

						blocksLevel[aux_blocks].properties.width = width - 20;
						blocksLevel[aux_blocks].properties.height = height - 10;

						aux_blocks++;
					}
				}
				aux_color++; //change color every time loop goes one row up
			}
		}

		void setTestPropertiesBlocks(const int screenWidth)
		{
			Color cant_colors_blocks[5]{ WHITE,RED,BLUE,GREEN,YELLOW };
			const short cant_filas = 5;
			const short width = 60;
			const short height = 20;
			const float y_mod = 1.5f;
			const short x_mod = 100;
			int aux_color = 0;

			//set j to the first y position, then add block height plus extra
			for (short j = GetScreenHeight() / y_mod; j < height * cant_filas + GetScreenHeight() / y_mod; j += height)
			{
				//same with i and x, but using width
				for (short i = screenWidth / x_mod; i < screenWidth - screenWidth / x_mod; i += width)
				{
					if (aux_blocks < maxblocks)
					{
						blocksLevel[aux_blocks].color = cant_colors_blocks[aux_color];
						blocksLevel[aux_blocks].active = true;

						blocksLevel[aux_blocks].properties.x = i;
						blocksLevel[aux_blocks].properties.y = j;

						blocksLevel[aux_blocks].properties.width = width - 20;
						blocksLevel[aux_blocks].properties.height = height - 10;

						aux_blocks++;
					}
				}
				aux_color++; //change color every time loop goes one row up
			}
		}

		void drawBlocks() 
		{
			for (short i = 0; i < aux_blocks; i++) 
			{
				if (blocksLevel[i].active)
				DrawRectangleRec(blocksLevel[i].properties, blocksLevel[i].color);
			}
		}
	}
}