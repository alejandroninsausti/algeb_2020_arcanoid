#include "Ball.h"
#include "../../Entities/Paddle/Paddle.h"
#include <math.h>

using namespace game;
using namespace gameplay;
using namespace paddle;
using namespace blocks;
namespace game
{
	namespace ball
	{
		Ball ball;
		float ballSpeed = 300;

		void generateBall()
		{
			ball.position.x = paddle::paddle.body.x + paddle::paddle.body.width / 2; //middle of paddle
			ball.position.y = paddle::paddle.body.y - paddle::paddle.body.height - ball.radius * 1.2f; //just above paddle
			ball.speed.x = 0;
			ball.speed.y = 0;
			ball.angle = 90;
			ball.color = GRAY;
			ball.collided = false;
			ball.launched = false;
		}

		void checkCollisionWalls()
		{
			if (ball.position.x >= (GetScreenWidth() - ball.radius) || ball.position.x <= ball.radius)
			{
				ball.speed.x *= -1.0f;
			}
			if (ball.position.y <= ball.radius)
			{
				ball.speed.y *= -1.0f;
			}
			else if (ball.position.y + ball.radius >= GetScreenHeight()) //if ball went beyond paddle, reset ball
			{
				ball.speed.x = 0;
				ball.speed.y = 0;
				ball.launched = false;
				ball.position.x = paddle::paddle.body.x + paddle::paddle.body.width / 2;
				ball.position.y = paddle::paddle.body.y - paddle::paddle.body.height - ball.radius * 1.2f;
			}
		}

		void checkCollisionPaddle()
		{
			Vector2 p = getPaddlePerimeterPosition(paddle::paddle.body);
			if (checkCollisionPaddle(p))
			{
				if (p.y == paddle::paddle.body.y) //the ball collided at the top
				{
					if (p.x == paddle::paddle.body.x + (paddle::paddle.body.width / 2)) //the ball collided in middle of the paddle
					{
						ball.speed.x = 0;
					}
					else //the ball collided anywhere else (in X)
					{
						float porcX = 0; //https://docs.google.com/drawings/d/1CHrik1DExrjBGC5tdEOR9SrTnWfqqVNOzwQJmsBRLuY/edit?usp=sharing

						if (p.x < paddle::paddle.body.x + (paddle::paddle.body.width / 2)) //the ball collided at the left
						{
							porcX = //min. angle - percentage of ball's in half of paddle
								100 //minimum angle
								-
								(paddle::paddle.body.x + (paddle::paddle.body.width / 2) - ball.position.x)
								//ball's x position in left half's of paddle
								*
								100
								/
								(paddle::paddle.body.width / 2); //half of paddle
							setAngleForX(porcX);
							ball.speed = getVelocityForX(ball.angle, ballSpeed);

							ball.speed.x *= -1;//invert ball speed, because it's launched to the right
						}
						else //the ball collided at the right
						{
							porcX = //percentage of ball's in half of paddle
								(paddle::paddle.body.x + paddle::paddle.body.width - ball.position.x)
								//ball's x position in right half's of paddle
								*
								100
								/
								(paddle::paddle.body.width / 2); //half of paddle
							setAngleForX(porcX);
							ball.speed = getVelocityForX(ball.angle, ballSpeed);
						}
					}

					ball.position.y = paddle::paddle.body.y - ball.radius - 1; //move the ball up, so it doesn't clip with paddle
					ball.speed.y *= -1; //invert ball's y speed

					return;
				}

				changeDirection(p, paddle::paddle.body);
			}
		}

		void setAngleForX(float porcX)
		{
			float ballAngleMax = 90;
			float ballAngleMin = 20;

			ball.angle =
				ballAngleMin //minAngle is needed to stop the ball from bouncing in a angle of 0
				+
				(ballAngleMax - ballAngleMin) //total angle variation (from min to max)
				* porcX //angle variation * porcX = specific angle * 100
				/ 100; //is divided by 100 to get the real angle that will be used
		}

		Vector2 getVelocityForX(float angle, float velocity)
		{
			Vector2 auxVelocity = { 0, 0 }; //speed in x,y

			auxVelocity.x = // cosine * hypotenuse = adjacent (cos * ballSpeed = ballXSpeed) sohCAHtoa
				velocity //current speed
				*
				static_cast<float>(cos //cosine
				(
					static_cast<double>(angle) * PI / 180
					//multiply angle by PI/180 to turn it from radians to degrees
					//https://www.mathway.com/es/popular-problems/Precalculus/476925
					//360� = 2PI  radians || PIrad / 180 is like 1/1
				));

			auxVelocity.y = //pitagoras (ySpeed^2 = maxSpeed^2 - xSpeed^2) 
				static_cast<float>(sqrt
				(
					pow(static_cast<double>(velocity), 2)
					-
					pow(static_cast<double>(auxVelocity.x), 2)
				));

			return auxVelocity;
		}

		void checkCollisionBlocks()
		{
			Vector2 p = Vector2(); //collision point
			for (short i = 0; i < maxblocks; i++)
			{
				p = getPaddlePerimeterPosition(blocksLevel[i].properties);
				if (checkCollisionPaddle(p) && (blocksLevel[i].active))
				{
					blocksLevel[i].active = false; //block.active = false == block is destroyed

					changeDirection(p, blocksLevel[i].properties);
				}

			}
		}

		void changeDirection(Vector2 p, Rectangle paddle) //p = collision point, paddle = collided rectangle
		{
			if (p.y == paddle.y) //Collision Up
			{
				ball.position.y = paddle.y - ball.radius * 1.05; //move ball so it doesn't clip

				if (ball.speed.y > 0)
				{
					ball.speed.y *= -1;
				}
			}
			else if (p.y == paddle.y + paddle.height) //Collision Down
			{
				ball.position.y = paddle.y + paddle.height + ball.radius * 1.05; //move ball so it doesn't clip

				if (ball.speed.y < 0)
				{
					ball.speed.y *= -1;
				}
			}

			if (p.x == paddle.x) //Collision Left
			{
				ball.position.x = paddle.x - ball.radius * 1.05; //move ball so it doesn't clip

				if (ball.speed.x > 0)
				{
					ball.speed.x *= -1;
				}
			}
			else if (p.x == paddle.x + paddle.width) //Collision Right
			{
				ball.position.x = paddle.x + paddle.width + ball.radius * 1.05; //move ball so it doesn't clip

				if (ball.speed.x < 0)
				{
					ball.speed.x *= -1;
				}
			}
		}

		bool checkCollisionPaddle(Vector2 p)
		{
			double distance = //pitagoras https://docs.google.com/drawings/d/1YzlRcOLiZRHFii777cNcYIS2lk1KpiYFsRID1qAYGbc/edit?usp=sharing
				sqrt
				(
					(static_cast<double>(ball.position.x) - static_cast<double>(p.x))
					*
					(static_cast<double>(ball.position.x) - static_cast<double>(p.x))
					//(ball's x - perimeter's x)^2
					+
					//(ball's y - perimeter's y)^2
					(static_cast<double>(ball.position.y) - static_cast<double>(p.y))
					*
					(static_cast<double>(ball.position.y) - static_cast<double>(p.y))
				);

			return distance < ball.radius;
		}

		Vector2 getPaddlePerimeterPosition(Rectangle paddle) //returns the x y position of the paddle that is closest to the ball 
															//(the collision point, in other words)
		{
			Vector2 p = Vector2();

			p.x = ball.position.x; //if ball's x is in the middle of paddle, leave it as it is
			if (p.x < paddle.x) //if ball's x is to the left of paddle, set p's x to paddle's left x
			{
				p.x = paddle.x;
			}
			else if (p.x > paddle.x + paddle.width) //if it is to the right, set p's x to paddle's right x
			{
				p.x = paddle.x + paddle.width;
			}

			//same with ball's y and paddle's y/height
			p.y = ball.position.y;
			if (p.y < paddle.y)
			{
				p.y = paddle.y;
			}
			else if (p.y > paddle.y + paddle.height)
			{
				p.y = paddle.y + paddle.height;
			}

			return p;
		}

		void changeVelocityForGravity()
		{
			//float vf = sqrt(pow(ballSpeed, 2) + 2 * test::g * (paddle::paddle.body.y - ball.radius));
		}

		void moveBall() //self-explanatory
		{
			ball.position.x += ball.speed.x * GetFrameTime();
			ball.position.y += ball.speed.y * GetFrameTime();
		}

		void drawBall() //also self-explanatory
		{
			DrawCircleV(ball.position, ball.radius, ball.color);
		}

		void checkLaunch()
		{
			if (IsKeyPressed(KEY_SPACE))
			{
				ball.launched = true;
				ball.speed.y -= ballSpeed;
			}
			else
			{
				followPaddle();
			}
		}

		void followPaddle() //ball's x stays in middle of paddle
		{
			ball.position.x = paddle::paddle.body.x + paddle::paddle.body.width / 2;
		}
	}
}