#include "Gameplay.h"
#include "../../Game/Game.h"

namespace game {
	namespace gameplay {

		void init()
		{
			game::paddle::GeneratePaddle();
			game::blocks::setPropertiesBlocks(game::screenWidth);
			game::ball::generateBall();
		}

		void update()
		{
			game::paddle::MovePaddle();
			if (!game::ball::ball.launched)
			{
				game::ball::checkLaunch();
			}
			else
			{
				game::ball::moveBall();
			}
			
			game::ball::checkCollisionBlocks();
			game::ball::checkCollisionPaddle();
			game::ball::checkCollisionWalls();

			if (IsKeyPressed(KEY_P)) {
				pause_menu::Init(true);
				gameStatus = GAME_STATUS::PAUSE;
			}
		}

		void draw()
		{
			game::paddle::ShowPaddle();
			game::ball::drawBall();
			game::blocks::drawBlocks();
		}

		void deInit()
		{
		}
	}

	namespace test
	{
		const float g = 9.81f; //earth's gravity

		void init()
		{
			game::paddle::GeneratePaddle();
			game::blocks::setTestPropertiesBlocks(game::screenWidth);
			game::ball::generateBall();
		}

		void freeFall()
		{
			/*
			* this should technicaly be:
			* "ball::ball.speed.y += 15 * (g - 0/ball.mass) * GetFrameTime();"
			* but, taking in account we don't have air resistance, it was seen unnecesary adding
			* mass to the ball.
			*/
			ball::ball.speed.y += 15 * g * GetFrameTime(); //10 is used to compensate the 0.00... of FrameTime
		}

		void update()
		{
			game::paddle::MovePaddle();
			if (!game::ball::ball.launched)
			{
				game::ball::checkLaunch();
			}
			else
			{
				freeFall();
				game::ball::moveBall();
			}

			game::ball::checkCollisionBlocks();
			game::ball::checkCollisionPaddle();
			game::ball::checkCollisionWalls();

			if (IsKeyPressed(KEY_P)) {
				pause_menu::Init(true);
				gameStatus = GAME_STATUS::PAUSE;
			}
		}

		void draw()
		{
			game::paddle::ShowPaddle();
			game::ball::drawBall();
			game::blocks::drawBlocks();
		}

		void deInit()
		{
		}
	}
}